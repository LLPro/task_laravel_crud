<!-- Create Modal HTML -->
@section('create_view')
<div id="addEmployeeModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {{Form::open(['action' => 'UsersController@store'])}}
                <div class="modal-header">
                    <h4 class="modal-title">Add Users</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        {{Form::text('name')}}
                        <!--input type="text" class="form-control" required-->
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        {{Form::text('email')}}
                        <!--input type="email" class="form-control" required-->
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        {{Form::text('address')}}
                        <!--textarea class="form-control" required></textarea-->
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        {{Form::text('phone')}}
                        <!--input type="text" class="form-control" required-->
                    </div>
                </div>
                <div class="modal-footer">
                    {{Form::submit('Send',['class'=>'btn btn-success'])}}
                    <!--input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel"-->
                    <!--button class="btn btn-success" type="button">Add</button-->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
